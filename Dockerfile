FROM node:10-slim

# ENV config to permit global install
ENV NPM_CONFIG_PREFIX=/home/node/.npm-global
ENV PATH=$PATH:/home/node/.npm-global/bin

# Set to a non-root built-in user `node`
USER node

# Create app directory
RUN mkdir -p /home/node/app
WORKDIR /home/node/app

# Bundle app source code
COPY --chown=node . .

# Install ionic-cli
RUN npm install -g @ionic/cli

# Install app dependencies
COPY --chown=node package*.json ./
RUN npm install

# Serve app
CMD ionic serve