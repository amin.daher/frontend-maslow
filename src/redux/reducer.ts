const defaultState = {
    user : {} , 
    token : {}
}


export default function reducer(
    state = defaultState,
    { type, payload }: { type: string, payload: any }
): any {
    
    // work with state
    switch (type) {
        case 'SET_USER_STATE':
            return {
                ...state,
                user:{
                    id : payload.id,
                    email : payload.email,
                    lastname : payload.lastname,
                    firstname : payload.firstname,
                    roles : payload.roles
                } 
            }
            case 'SET_TOKEN' : 
            return {
                ...state,
                token:{
                    token : payload
                } 
            }
    }

    return state
}

