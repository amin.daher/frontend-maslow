import { API_URL, HOST, PORT } from './env.service'
import axios from 'axios'


export async function GetLocks(token: any): Promise<any> {
    return new Promise((resolve, reject) => {
        axios.get(API_URL + '/locks', { headers: { "Authorization": `Bearer ${token}` } })
            .then(response => {                      
                resolve(response.data)
            }).catch(error => {
                resolve(false)
            })
    })
}

export async function GetLock(token: string, id: string): Promise<any> {
    return new Promise((resolve, reject) => {
        axios.get(API_URL + '/locks/' + id, { headers: { "Authorization": `Bearer ${token}` } })
            .then(response => {                
                resolve(response.data.data)
            }).catch(error => {
                resolve(false)
            })
    })
}

export async function OpenLock(token: string, id: string): Promise<any> {
    return new Promise((resolve, reject) => {
        axios.post(API_URL + '/locks/open/' + id, { headers: { "Authorization": `Bearer ${token}` } })
            .then(response => {
                resolve(response.data)
            }).catch(error => {
                resolve(false)
            })
    })
}

export async function CloseLock(token: string, id: string): Promise<any> {
    return new Promise((resolve, reject) => {
        axios.post(API_URL + '/locks/close/' + id, { headers: { "Authorization": `Bearer ${token}` } })
            .then(response => {
                resolve(response.data)
            }).catch(error => {
                resolve(false)
            })
    })
}