import { API_URL, HOST, PORT } from './env.service'
import axios from 'axios'

export async function addThingAccess(token: string, body : any): Promise<any> {
    return new Promise((resolve, reject) => {
    axios.post(API_URL + '/thing-accesses', body ,{ headers: { "Authorization": `Bearer ${token}` } })
        .then(response => {                            
            resolve(response.data)
        }).catch(error => {
        resolve(false)
        })
    })
}

export async function deleteThingAccess(token: string, id : string): Promise<any> {
    return new Promise((resolve, reject) => {
    axios.delete(API_URL + '/thing-accesses/'+ id ,{ headers: { "Authorization": `Bearer ${token}` } })
        .then(response => {                            
            resolve(response.data)
        }).catch(error => {
        resolve(false)
        })
    })
}