import { API_URL, HOST, PORT } from './env.service'
import axios from 'axios'


export async function GetThings(token: string): Promise<any> {

    return new Promise((resolve, reject) => {
        axios.get(API_URL + '/things', { headers: { "Authorization": `Bearer ${token}` } })
            .then(response => {   
                resolve(response.data)
            }).catch(error => {
                resolve(false)
            })
    })
}

export async function GetThing(token: string , uid : string): Promise<any> {

    return new Promise((resolve, reject) => {
        axios.get(API_URL + '/things/' + uid, { headers: { "Authorization": `Bearer ${token}` } })
            .then(response => {   
                resolve(response.data)
            }).catch(error => {
                resolve(false)
            })
    })
}

export async function GetItem(token: string , name : string): Promise<any> {

    return new Promise((resolve, reject) => {
        axios.get(API_URL + '/items/' + name, { headers: { "Authorization": `Bearer ${token}` } })
            .then(response => {   
                resolve(response.data)
            }).catch(error => {
                resolve(false)
            })
    })
}

export async function UpdateItem(token: string , name : string , value : string): Promise<any> {

    return new Promise((resolve, reject) => {
        axios.post(API_URL + '/items/' + name, {itemValue : value } , { headers: { "Authorization": `Bearer ${token}` } })
            .then(response => {   
                resolve(response.data)
            }).catch(error => {
                resolve(false)
            })
    })
}
