import { API_URL, HOST, PORT } from './env.service'
import axios from 'axios'
import { Toast } from '../toast';

export async function FirstLogin(email: string, password: string): Promise<any> {

    var body = {
        email: email,
        password: password
    }

    return new Promise((resolve, reject) => {
        axios.post(API_URL + '/users/login', body)
            .then(response => {
                resolve(response.data.token)
            }).catch(error => {
                Toast('Echec d\'identification')  
                resolve(false)              
            })
        })
}