import { API_URL, HOST, PORT } from './env.service'
import axios from 'axios'
import { Toast } from '../toast'

import { GetSessionToken , GetSessionUser, SetSessionToken, SetSessionUser} from '../session'

export async function CurrentUser(token: string): Promise<any> {

    return new Promise((resolve, reject) => {
        axios.get(API_URL + '/users/me', { headers: { "Authorization": `Bearer ${token}` } })
            .then(response => {                            
                resolve(response.data)
            }).catch(error => {                            
                resolve(false)
            })
    })
    
}




export async function ListUsers(token: string): Promise<any> {
    return new Promise((resolve, reject) => {
        axios.get(API_URL + '/users', { headers: { "Authorization": `Bearer ${token}` }})
            .then(response => { 
                resolve(response.data)
            }).catch(error => {
                resolve(false)
            })
    })
}


export async function DetailUser(token: string, id : any): Promise<any> {
    return new Promise((resolve, reject) => {
        axios.get(API_URL + '/users/'+ id, { headers: { "Authorization": `Bearer ${token}` }})
            .then(response => {                                            
                resolve(response.data)
            }).catch(error => {
                resolve(false)
            })
    })
}


export async  function LogoutUser(){         
    SetSessionToken(null)
    SetSessionUser(null)
    Toast("Déconnection avec succés")
}
