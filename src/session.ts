export function SetSessionToken(token: any) {
    // local storage est mieux les information reste meme en fermant le navigator
    sessionStorage.setItem('token', token)
}
export function SetSessionUser(user: any) {
    sessionStorage.setItem('user', JSON.stringify(user))
}
export function GetSessionToken() {
    let token = sessionStorage.getItem('token')
    return token
}
export function GetSessionUser() {
    let data : any = sessionStorage.getItem('user')
    let user = JSON.parse(data)
    return user
}