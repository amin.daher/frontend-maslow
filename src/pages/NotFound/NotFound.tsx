import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar } from '@ionic/react';
import React from 'react';
import './NotFound.css';


const NotFound: React.FC = () => {


  return (
    <IonPage className="ion-padding">

      <IonHeader>
        <IonToolbar>
          <IonTitle>NotFound page</IonTitle>
        </IonToolbar>
      </IonHeader>

      <IonContent>


        <IonHeader collapse="condense">
          <IonToolbar>
            <IonTitle size="large">Blank</IonTitle>
          </IonToolbar>
        </IonHeader>

      </IonContent>
    </IonPage>
  );
};

export default NotFound;
