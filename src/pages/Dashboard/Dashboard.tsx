import {
  IonContent, IonHeader, IonPage, IonTitle, IonToolbar, IonList, IonItem,
  IonAvatar, IonLabel, IonItemSliding, IonItemOption, IonItemOptions, IonButton, IonInput, IonLoading, IonIcon, IonItemGroup, IonItemDivider
} from '@ionic/react';

import React, { useState, useRef, useEffect } from 'react';
import './Dashboard.css';
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router';

import { GetSessionToken, GetSessionUser } from '../../session'
import { LogoutUser } from '../../services/users.service'

import { lock, unlock } from 'ionicons/icons';
import { GetLocks, GetLock, OpenLock, CloseLock } from '../../services/lock.service'
import { GetThings } from '../../services/things.service';


const Dashboard: React.FC = () => {
  const user: any = GetSessionUser()
  const token: any = GetSessionToken()
  const [locks, setlocks] = useState<any>([])
  const [things, setthings] = useState<any>([])

  // pour mettre async
  useEffect(() => {
    getlocks();
    getthings();
  }, []);

  async function getlocks() {
    let tmp: any[] = await GetLocks(token)
    for (let i = 0; i < tmp.length; i++) {
      tmp[i].infos = await GetLock(token, tmp[i].id)
    }
    setlocks(tmp)
  }

  async function getthings() {
    let tmp: any[] = await GetThings(token)
    let res: any[] = []
    for (let i = 0; i < tmp.length; i++) {
      res.push({
        uid: tmp[i].UID,
        label: tmp[i].label
      })
    }
    setthings(res)
  }

  const history = useHistory()
  const [busy, setbusy] = useState<boolean>(false)

  async function logout() {
    setbusy(true)
    await LogoutUser()
    history.replace('/home')
    setbusy(false)
  }

  function Administration() {
    history.replace('/users')
  }

  const sleep = (milliseconds: any) => {
    return new Promise(resolve => setTimeout(resolve, milliseconds))
  }


  async function openLock(id: string) {
    await OpenLock(token, id)
    await sleep(10000) // probleme de serrure qui prends du temps pour repondre
    getlocks()
  }

  async function closeLock(id: string) {
    await CloseLock(token, id)
    await sleep(10000)
    getlocks()

  }

  function AccesItems(thingUid: any) {
    history.replace('/items/' + thingUid)
  }

  return (
    <IonPage className="ion-padding">
      <IonHeader>
        <IonToolbar>
          <IonTitle>Dashboard</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent >
        <IonLoading message="deconnection en cours ... " duration={0} isOpen={busy} />

        {(user !== null) ?
          <p className="para">
            User name : {user.email}
            <IonButton expand="full" onClick={logout}> Logout </IonButton>

            {user.roles.map((Element: any) => (
              (Element.roleId === 'ADMIN') ? <IonButton expand="full" onClick={Administration} color="success"> Administration </IonButton> : null
            ))}


          </p>
          : null}

        <IonList>
          <IonItemGroup>
            <IonItemDivider> <IonLabel> <h1>Listes des serrures</h1> </IonLabel> </IonItemDivider>
            {locks.map((Element: any) => (
              <IonItemSliding key={Element.id}>
                <IonItem >
                  <IonAvatar>
                    <img src="https://cdn0.iconfinder.com/data/icons/internet-of-things-iot-1/68/Door_door_lock_lock_network_remote_access_wifi_icon-512.png" />
                  </IonAvatar>
                  {(Element.infos.etat === 'close') ?
                    <IonIcon icon={lock} size="large" slot="end">  </IonIcon> :
                    <IonIcon icon={unlock} size="large" slot="end">  </IonIcon>
                  }

                  <IonLabel className="ion-padding" >
                    <h2>{Element.infos.nom}
                    </h2>
                  </IonLabel>

                </IonItem>

                {(Element.infos.etat === 'close') ?
                  <IonItemOptions side="end">
                    <IonItemOption onClick={() => openLock(Element.id)} color="primary">OPEN</IonItemOption>
                  </IonItemOptions>
                  :
                  <IonItemOptions side="end">
                    <IonItemOption onClick={() => closeLock(Element.id)} color="primary">CLOSE</IonItemOption>
                  </IonItemOptions>
                }
              </IonItemSliding>
            ))}
          </IonItemGroup>
          <br></br>
          <br></br>
          <br></br>
          <br></br>
          <IonItemGroup>
            <IonItemDivider> <IonLabel> <h1>Liste des objets connectés (Hors serrures)</h1> </IonLabel> </IonItemDivider>
            {things.map((Element: any) => (
              <IonItemSliding key={Element.uid}>
                <IonItem >
                  <IonAvatar>
                    <img src="https://www.digitalunite.com/sites/default/files/images/shutterstock_140577157.jpg" />
                  </IonAvatar>

                  <IonLabel className="ion-padding" >
                    <h2>{Element.label}</h2>
                  </IonLabel>

                </IonItem>

                <IonItemOptions side="end">
                  <IonItemOption onClick={() => AccesItems(Element.uid)} color="primary">Options</IonItemOption>
                </IonItemOptions>

              </IonItemSliding>
            ))}
          </IonItemGroup>
        </IonList>
      </IonContent>
    </IonPage>
  );
};

export default Dashboard;
