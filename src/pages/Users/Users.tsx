import {
  IonContent, IonHeader, IonPage, IonTitle, IonToolbar, IonList, IonItem,
  IonAvatar, IonLabel, IonItemSliding, IonItemOption, IonItemOptions, IonButton, IonInput, IonLoading, IonRow, IonGrid, IonCol
} from '@ionic/react';

import React, { useState, useRef, useEffect } from 'react';
import './Users.css';
import { useHistory, RouteComponentProps } from 'react-router';
import { SetSessionToken, SetSessionUser, GetSessionToken, GetSessionUser } from '../../session';
import { ListUsers } from '../../services/users.service';
import { FirstLogin } from '../../services/auth.service';



const Users: React.FC = () => {

  const user: any = GetSessionUser()
  const token: any = GetSessionToken()

  const [users, setUsers] = useState<any>([])
  const history = useHistory()


  // pour mettre async
  useEffect(() => {
    getUsers();
  }, []);


  async function getUsers() {
    if (token) {
      let tmp = await ListUsers(token)
      setUsers(tmp);
    }
  }

  function Dashboard() {
    history.replace('/dashboard')
  }

  function Detail(id : string ) {
    history.replace('/accesses/' + id)
  }

  return (

    <IonPage className="ion-padding">

      <IonHeader>
        <IonToolbar>
          <IonTitle>Liste des utilisateurs</IonTitle>
        </IonToolbar>
      </IonHeader>


      <IonContent >

        {(user !== null) ?
          <p className="para">
            User name : {user.email}
            <IonButton expand="full" onClick={Dashboard}> Dashboard </IonButton>
          </p>
          : null}



        <IonGrid className="tab">

          <IonRow className="firstRow">
            <IonCol>Nom</IonCol>
            <IonCol>Role</IonCol>
            <IonCol>Email</IonCol>
            <IonCol>Details</IonCol>
          </IonRow>

          {users.map((element: any) => (
            
            (element.roles[0].roleId !== 'ADMIN') ?
            <IonRow key={element.id} className="ligne">
              <IonCol>{element.lastname}</IonCol>
              <IonCol>{element.firstname}</IonCol>
              <IonCol>{element.email}</IonCol>
              <IonCol> <a onClick={() => Detail(element.id)}   >Détails</a> </IonCol>
            </IonRow>
            : null
            ))
          }

        </IonGrid>

      </IonContent>
    </IonPage>
  );
};

export default Users;
