import {
  IonContent, IonHeader, IonPage, IonTitle, IonToolbar, IonList, IonItem,
  IonAvatar, IonLabel, IonItemSliding, IonItemOption, IonItemOptions, IonButton, IonInput, IonLoading, IonCard, IonRange, IonIcon, IonItemGroup, IonItemDivider, IonToggle, IonCheckbox
} from '@ionic/react';

import React, { useState, useRef, useEffect } from 'react';
import './Items.css';
import { useSelector } from 'react-redux';
import { useHistory, Router, RouteComponentProps } from 'react-router';

import { thermometer, contrast } from 'ionicons/icons'

import { GetSessionToken, GetSessionUser } from '../../session'
import { LogoutUser } from '../../services/users.service'
import { GetThing, GetItem, UpdateItem } from '../../services/things.service';

interface PageProps extends RouteComponentProps<{
  thingUid: string;
}> { }


const Items: React.FC<PageProps> = ({ match }) => {

  const user: any = GetSessionUser()
  const token: any = GetSessionToken()
  const [items, setitems] = useState<any>([])
  const uid = match.params.thingUid
  const [thingname, setthingname] = useState<any>([])

  // pour mettre async
  useEffect(() => {
    getitems();
  }, []);


  async function getitems() {
    let thing: any = await GetThing(token, uid)
    let channels: any[] = thing.channels
    let tmp: any = []

    setthingname(thing.label)

    for (let i = 0; i < channels.length; i++) {
      let item = channels[i].linkedItems[0]
      let defaultTags = channels[i].defaultTags
      let itemsInfos = await GetItem(token, item)
      tmp.push({
        name: item,
        defaultTags: defaultTags,
        itemInfos: itemsInfos
      })
    }
    setitems(tmp)
  }


  async function updateitems(name: string, value: string) {
    UpdateItem(token, name, value)
    await sleep(3000)    // probleme de temps de reponse apres le changement de state d'un objet.
    getitems()
  }

  const history = useHistory()
  const [busy, setbusy] = useState<boolean>(false)

  async function logout() {
    setbusy(true)
    await LogoutUser()
    history.replace('/home')
    setbusy(false)
  }

  const sleep = (milliseconds: any) => {
    return new Promise(resolve => setTimeout(resolve, milliseconds))
  }

  function Dashboard() {
    history.replace('/dashboard')
  }

  return (
    <IonPage className="ion-padding">
      <IonHeader>
        <IonToolbar>
          <IonTitle>Option de {thingname} </IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent >

        <IonLoading message="deconnection en cours ... " duration={0} isOpen={busy} />




        {(user !== null) ?
          <p className="para">
            User name : {user.email}
            <IonButton expand="full" onClick={Dashboard}> Dashboard </IonButton>

          </p>
          : null}


        <IonList>

          {items.map((Element: any) => (

            <IonItemGroup>

              <IonItemDivider> <IonLabel> <h1>{Element.itemInfos.label}</h1> </IonLabel> </IonItemDivider>

              {(Element.defaultTags.length !== 0) ?

                Element.defaultTags.map((i: any) => (
                  (i === "Lighting") ?
                    <div>
                      <IonItem key={Element.name}>
                        <IonRange min={0} max={100} value={Element.itemInfos.state} step={1} ticks={false} snaps={true} pin color="secondary"
                          onIonChange={(e: any) => updateitems(Element.name, e.target.value)} >
                          <IonIcon slot="start" size="small" icon={contrast}></IonIcon>
                          <IonIcon slot="end" icon={contrast}></IonIcon>
                        </IonRange>
                      </IonItem>

                      <IonItem>
                        <IonLabel> ON/OFF </IonLabel>
                        <IonCheckbox slot="start" color="secondary" checked={(Element.itemInfos.state !== 0)}
                          onIonChange={(e: any) => updateitems(Element.name, (e.detail.checked ? '100' : '0'))}></IonCheckbox>

                      </IonItem>

                    </div>
                    : null

                ))

                : null
              }

              {(Element.defaultTags.length === 0 && Element.itemInfos.type === 'Dimmer') ?
                <IonItem key={Element.name}>
                  <IonRange min={0} max={100} step={1} value={Element.itemInfos.state} ticks={false} snaps={true} pin color="secondary"
                    onIonChange={(e: any) => updateitems(Element.name, e.target.value)}>
                    <IonIcon slot="start" size="small" color="danger" icon={thermometer}></IonIcon>
                    <IonIcon slot="end" color="danger" icon={thermometer}></IonIcon>
                  </IonRange>
                </IonItem>
                : null
              }

              {(Element.defaultTags.length === 0 && Element.itemInfos.type === 'Number') ?
                <IonItem key={Element.name}>
                  <IonLabel className="centrer" > {Element.itemInfos.state} </IonLabel>
                </IonItem>
                : null
              }

            </IonItemGroup>

          ))}
        </IonList>


      </IonContent>
    </IonPage>
  );
};

export default Items;
