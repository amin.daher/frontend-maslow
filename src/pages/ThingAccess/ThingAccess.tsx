import {
  IonContent, IonHeader, IonPage, IonTitle, IonToolbar, IonList, IonItem,
  IonAvatar, IonLabel, IonItemSliding, IonItemOption, IonItemOptions, IonButton, IonInput, IonLoading, IonRow, IonGrid, IonCol, IonButtons, IonSelect, IonDatetime, IonSelectOption
} from '@ionic/react';


import { Toast } from '../../toast'



import { useForm, Controller } from "react-hook-form";
import React, { useState, useRef, useEffect } from 'react';
import './ThingAccess.css';
import { useHistory, RouteComponentProps } from 'react-router';
import { addThingAccess, deleteThingAccess } from '../../services/thingAccesses.service';
import { GetSessionToken, GetSessionUser } from '../../session';
import { DetailUser } from '../../services/users.service';
import { GetThings } from '../../services/things.service';

let initialValues = {
  thing_uid: "",
  start_date: "",
  end_date: "",
};


interface PageProps extends RouteComponentProps<{
  id: string;
}> { }


const ThingAccess: React.FC<PageProps> = ({ match }) => {


  const user: any = GetSessionUser()


  const token: any = GetSessionToken()
  const id = match.params.id
  const history = useHistory()
  let [detailUser, setDetailUser] = useState<any>({ thingsAccesses: [] })

  const [things, setthings] = useState<any>([])


  const { control, register, handleSubmit, errors, formState } = useForm({
    defaultValues: initialValues
  });

  // pour mettre async
  useEffect(() => {
    getUser();
    getthings();
  }, []);

  function SupprimerAccess(id: string) {
    if (deleteThingAccess(token, id)) {
      Toast("Suppression Réussite")
      getUser();
    }

  }


  async function getthings() {
    let tmp: any[] = await GetThings(token)
    let res: any[] = []
    for (let i = 0; i < tmp.length; i++) {
      res.push({
        uid: tmp[i].UID,
        label: tmp[i].label
      })
    }
    setthings(res)
  }

  function Administration() {
    history.replace('/users')
  }

  async function getUser() {
    if (token) {
      let tmp = await DetailUser(token, id);
      setDetailUser(tmp);
    }
  }

  const onSubmit = (data: any) => {
    data.userId = detailUser.id;
    if (token) {
      if (addThingAccess(token, data)) {
        Toast("Ajout Réussi")
        getUser();
      }
    }
  };


  return (
    <IonPage className="ion-padding">

      <IonHeader>
        <IonToolbar>
          <IonTitle>Affectation objets</IonTitle>
        </IonToolbar>
      </IonHeader>

      <IonContent >

        {(user !== null) ?
          <p className="para">
            User name : {user.email}
            <IonButton expand="full" onClick={Administration} > Retour </IonButton>

          </p>
          : null}

        <IonGrid className="tab">
          <IonRow className="firstRow">
            <IonCol>Objet</IonCol>
            <IonCol>Début d'accès</IonCol>
            <IonCol>Fin d'accès</IonCol>
            <IonCol>  </IonCol>
          </IonRow>

          {detailUser.thingsAccesses.map((element: any) => (

            <IonRow className="ligne">
              <IonCol>{element.thing_uid}</IonCol>
              <IonCol>{element.start_date}</IonCol>
              <IonCol>{element.end_date}</IonCol>
              <IonCol><a onClick={() => SupprimerAccess(element.id)}   >Supprimer</a></IonCol>
            </IonRow>))

          }

        </IonGrid>



        <form onSubmit={handleSubmit(onSubmit)} >
          <IonList>

            <IonItem>
              <IonLabel>ID de l'objet: </IonLabel>
              {/* <IonInput name="thing_uid" type="text" ref={register({ required: true })} /> */}
              <IonSelect name="thing_uid" ref={register({ required: true })} >
                {things.map((Element: any) =>
                  <IonSelectOption value={Element.uid}>{Element.label}</IonSelectOption>
                )}
              </IonSelect>

            </IonItem>

            <IonItem>
              <IonLabel>Début d'accès : </IonLabel>
              <IonDatetime name="start_date" ref={register({ required: true })} />
            </IonItem>

            <IonItem>
              <IonLabel>Fin d'accès : </IonLabel>
              <IonDatetime name="end_date" ref={register({ required: true })} />
            </IonItem>

          </IonList>

          <IonButton expand="full" type="submit">Ajouter nouveau accès</IonButton>
        </form>



      </IonContent>
    </IonPage>
  );
};

export default ThingAccess;
