import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar, IonInput, IonButton, IonLoading } from '@ionic/react';
import React, { useState } from 'react';
import './Login.css';
import { Link, useHistory } from 'react-router-dom';
import { Toast } from '../../toast'
import { setUserState } from '../../redux/action';
import { setToken } from '../../redux/action';
import { useDispatch } from 'react-redux';


import { SetSessionToken , SetSessionUser } from '../../session'


import { FirstLogin } from '../../services/auth.service'
import { CurrentUser } from '../../services/users.service'

const Login: React.FC = () => {
 
  const [username, setUsername] = useState('') // '' initialise
  const [password, setPassword] = useState('')

  const history = useHistory()
  const [busy, setbusy] = useState<boolean>(false)


  async function login() {
    setbusy(true)
    const token: any = await FirstLogin(username, password)

    if (token) {

      SetSessionToken(token)
      
      const user : any = await CurrentUser(token)

      SetSessionUser(user)

      // faire la vérification pour passer soit en administration ou  dashboard
      history.replace('/dashboard')

      Toast('Login succes')
    }

    setbusy(false)

  }

  return (
    <IonPage className="ion-padding">

      <IonHeader>
        <IonToolbar>
          <IonTitle>Connexion</IonTitle>
        </IonToolbar>
      </IonHeader>

      <IonLoading message="Connextion en cours ... " duration={0} isOpen={busy} />

      <IonContent>
        <IonInput placeholder="Email" required onIonChange={(e: any) => setUsername(e.target.value)}></IonInput>
        <IonInput placeholder="Password" required type="password" onIonChange={(e: any) => setPassword(e.target.value)}></IonInput>
        <IonButton onClick={login} expand="full"> Log in</IonButton>        
      </IonContent>
      
    </IonPage>
  );
};

export default Login;
